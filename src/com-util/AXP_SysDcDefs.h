/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 23-Feb-2020 Jonathan D. Belanger
 *  Initially written.
 */

#ifndef _AXP_SYSDCDEFS_H_
#define _AXP_SYSDCDEFS_H_

#include "com-util/AXP_SysDc.h"

void
AXP_SysDc_Build_Rq(AXP_SysDc_Msg *msg, AXP_SysDc_Command command,
                   AXP_SysDc_Mask mask, u64 pa, u8 id, bool m1, bool m2,
                   bool rv, bool ch);
void
AXP_SysDc_Build_ProbeRsp(AXP_SysDc_Msg *msg, AXP_SysDc_Status status, u8 vdb,
                         u8 maf, bool dm, bool vs, bool ms);

void
AXP_SysDc_Build_Probe(AXP_SysDc_Msg *msg, AXP_SysDc_Probe_DM dm,
                      AXP_SysDc_Probe_Next ns, AXP_SysDc_SysDc sysDc, u64 pa,
                      u8 id, bool rvb, bool rpb, bool a, bool c);

void
AXP_SysDc_Build_DataXfer(AXP_SysDc_Msg *msg, AXP_SysDc_SysDc sysDc, u8 id,
                         bool rvb, bool rpb, bool a, bool c);

#endif /* _AXP_SYSDCDEFS_H_ */
