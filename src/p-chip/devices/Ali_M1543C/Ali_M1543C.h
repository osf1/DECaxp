/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions to emulate the Ali M1542C chipset
 *  used in the Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 26-Apr-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _ALI_M1543C_DEFS_
#define _ALI_M1543C_DEFS_

#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_PCI.h"

#define AXP_M1543C_DEVICE_ID    0x1533
#define AXP_M1543C_REV_ID       0xc3

typedef struct
{
} AXP_Ali_Current_Address;
typedef struct
{
} AXP_Ali_Current_Count;
typedef struct
{
} AXP_Ali_Status_Command;
typedef struct
{
} AXP_Ali_Write_Request;
typedef struct
{
} AXP_Ali_Write_Single_Mask;
typedef struct
{
} AXP_Ali_Write_Mode;
typedef struct
{
} AXP_Ali_Clear_Byte_Pointer;
typedef struct
{
} AXP_Ali_Master_Clear;
typedef struct
{
} AXP_Ali_Clear_Mask;
typedef struct
{
} AXP_Ali_RW_All_Mask_Register;
typedef struct
{
} AXP_Ali_Control_Register;
typedef struct
{
} AXP_Ali_Control_Mask;
typedef struct
{
} AXP_Ali_EdgeLevel_Control_Register;
typedef struct
{
} AXP_Ali_Timer_Counter;
typedef struct
{
} AXP_Ali_Timer_Mode;
typedef union
{
    u8 ctrl;
    struct
    {
        u8 strobe : 1;
        u8 autofeed : 1
        u8 initiate : 1;
        u8 select : 1;
        u8 interrupt_req : 1;
        u8 direction : 1;
        u8 res_1 : 2;
    };
} AXP_Ali_Device_Control_Register;
typedef struct
{
    u8 status;
    struct
    {
        u8 res_1 : 3;
        u8 fault : 1;
        u8 select : 1;
        u8 error : 1;
        u8 ack : 1;
        u8 not_busy : 1;
    };
} AXP_Ali_Device_Status_Register;

/*
 * Serial Port Registers
 *
 * Register Address:    0h
 * Access (AEN=0):      DLAB 0
 * Abbreviation:        THR
 * Register Name:       Transmit Holding Register
 * Access:              Write
 */
typedef u8 AXP_Ali_THR;

/*
 * Register Address:    0h
 * Access (AEN=0):      DLAB 0
 * Abbreviation:        RBR
 * Register Name:       Receive Buffer Register
 * Access:              Read
 */
typedef u8 AXP_Ali_RBR;

/*
 * Register Address:    0h
 * Access (AEN=0):      DLAB 1
 * Abbreviation:        DLL
 * Register Name:       Divisor Latch LSB
 * Access:              Read/Write
 */
typedef u8 AXP_Ali_DLL;

/*
 * Register Address:    1h
 * Access (AEN=0):      DLAB 1
 * Abbreviation:        DLM
 * Register Name:       Divisor Latch MSB
 * Access:              Read/Write
 */
typedef u8 AXP_Ali_DLM;

/*
 * Register Address:    1h
 * Access (AEN=0):      DLAB 0
 * Abbreviation:        IER
 * Register Name:       Interrupt Enable Register
 * Access:              Read/Write
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 rcv_data_avail_inter : 1;
        u8 THRE_inter : 1
        u8 rcvr_line_status_inter : 1;
        u8 modem_status_inter : 1;
        u8 res_1 : 4;
    };
} AXP_Ali_IER;

/*
 * Register Address:    2h
 * Access (AEN=0):      -
 * Abbreviation:        IIR
 * Register Name:       Interrupt Identification Register
 * Access:              Read
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 no_inter_pending : 1;
        u8 high_inter_pending : 2;
        u8 timeout_pending : 1;
        u8 res_1 : 2;
        u8 FCR_bit_0_set_1 : 1;
        u8 FCR_bit_0_set_2 : 1;
    };
} AXP_Ali_IIR;

/*
 * Register Address:    2h
 * Access (AEN=0):      -
 * Abbreviation:        FCR
 * Register Name:       FIFO Control Register
 * Access:              Write
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 enable_fifo : 1;
        u8 clear_rcvr_fifo : 1;
        u8 clear_xmit_fifo : 1;
        u8 res_1 : 3;
        u8 rcvr_inter_trigger_lvl : 2;
    };
} AXP_Ali_FCR;
#define AXP_Ali_RCVR_FIFO_LVL_01    0
#define AXP_Ali_RCVR_FIFO_LVL_04    1
#define AXP_Ali_RCVR_FIFO_LVL_08    2
#define AXP_Ali_RCVR_FIFO_LVL_14    3

/*
 * Register Address:    3h
 * Access (AEN=0):      -
 * Abbreviation:        LCR
 * Register Name:       Line Control Register
 * Access:              Read/Write
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 data_bits : 2;
        u8 stop_bits : 1;
        u8 parity_enable : 1;
        u8 parity_select :1;
        u8 parity_sticky : 1;
        u8 break_ctrl : 1;
        u8 DLAB : 1;
    };
} AXP_Ali_LCR;
#define AXP_Ali_5_BITS  0
#define AXP_Ali_6_BITS  1
#define AXP_Ali_7_BITS  2
#define AXP_Ali_8_BITS  3

/*
 * Register Address:    4h
 * Access (AEN=0):      -
 * Abbreviation:        MCR
 * Register Name:       Modem Control Register
 * Access:              Read/Write
 */
typedef union
{
    u8 reg;
    struct
    {
            u8 DTRJ_force_0 : 1;
            u8 RTSJ_force_0 : 1;
            u8 OUT1 : 1;
            u8 interrupt_enable : 1;
            u8 loopback : 1;
            u8 res_1 : 3;
    };
} AXP_Ali_MCR;

/*
 * Register Address:    5h
 * Access (AEN=0):      -
 * Abbreviation:        LSR
 * Register Name:       Line Status Register
 * Access:              Read
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 rcv_data_ready : 1;
        u8 overrun_error : 1;
        u8 parity_error : 1;
        u8 framing_error : 1;
        u8 break_interrupt : 1;
        u8 THR_empty : 1;
        u8 TEMT_empty : 1;
        u8 error_present : 1;
    };
} AXP_Ali_LSR;

/*
 * Register Address:    6h
 * Access (AEN=0):      -
 * Abbreviation:        MSR
 * Register Name:       Modem Status Register
 * Access:              Read
 */
typedef union
{
    u8 reg;
    struct
    {
        u8 DCTS : 1;
        u8 DDSR : 1;
        u8 TERI : 1;
        u8 DDCD : 1;
        u8 not_CTSJ : 1;
        u8 not_DSRJ : 1;
        u8 not_RIJ : 1;
        u8 not_DCDJ : 1;
    };
} AXP_Ali_MSR;

/*
 * Register Address:    7h
 * Access (AEN=0):      -
 * Abbreviation:        SCR
 * Register Name:       Scratch PadRegister
 * Access:              Read/Write
 */
typedef u8 AXP_Ali_SCR;
typedef struct
{
    AXP_Ali_THR thr;
    AXP_Ali_RBR rbr;
    AXP_Ali_DLL dll;
    AXP_Ali_DLM dlm;
    AXP_Ali_IER ier;
    AXP_Ali_IIR iir;
    AXP_Ali_FCR fcr;
    AXP_Ali_LCR lcr;
    AXP_Ali_MCR mcr;
    AXP_Ali_LSR lsr;
    AXP_Ali_MSR msr;
    AXP_Ali_SCR scr;
    bool present;
} AXP_Ali_Serial_Port;

#define ALI_RTC_PORTS 4
#define ALI_SERIAL_PORTS 4
#define ALI_TIMER_COUNTERS 3
typedef struct
{
    u8 timer_channel_counter[ALI_TIMER_COUNTERS];       /* Ports 40h - 42h */
    u8 timer_command_mode;                              /* Port 43h */
    u8 nmi_status_control_reg;                          /* Port 61h */
    u8 rtc_ports[ALI_RTC_PORTS];                        /* Ports 70h - 73h */
    u8 printer_data;
    AXP_Ali_Device_Control_Register printer_ctrl;
    AXP_Ali_Device_Status_Register printer_status;
    AXP_PCI_ConfigData currentConfig;
    AXP_Ali_Serial_Port serial_port[ALI_SERIAL_PORTS]
    u32 devId;
} AXP_Ali_1543C;

void AXP_Ali_M1543C_init(AXP_21274_PCHIP *p, u32 devId);

#endif  /* _ALI_M1543C_DEFS_ */
