/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains the code to emulate the Ali M1542C chipset used
 *  in the Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 26-Apr-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "com-util/AXP_Trace.h"
#include "p-chip/devices/Ali_M1543C/Ali_M1543C.h"

static const AXP_PCI_ConfigData configBase =
{
    .type0.common.vendorID = AXP_PCI_ALADDIN_VENDOR_ID, /* Index 01h-00h, RO */
    .type0.common.deviceID = AXP_M1543C_DEVICE_ID,      /* Index 03h-02h, RO */
    .type0.common.command.ioSpace = 0x1,                /* Index 05h-04h, RO */
    .type0.common.command.memorySpace = 0x1,
    .type0.common.command.busMaster = 0x1,
    .type0.common.command.specialCycles = 0x1,
    .type0.common.command.memWriteInvalidEna = 0x0,
    .type0.common.command.vgaPaletteSnoop = 0x0,
    .type0.common.command.res_0 = 0x0,
    .type0.common.command.serrEna = 0x0,
    .type0.common.command.fastB2BEna = 0x0,
    .type0.common.command.iterruptDisable = 0x0,
    .type0.common.command.res_1 = 0x0,
    .type0.common.status.res_0 = 0x0,                   /* Index 07h-06h, RW */
    .type0.common.status.interruptStatus = 0x0,
    .type0.common.status.capabilitiesList = 0x0,
    .type0.common.status.sixtySixMhz = 0x0,
    .type0.common.status.res_1 = 0x0,
    .type0.common.status.fastB2BEna = 0x0,
    .type0.common.status.masterDataParityErr = 0x0,
    .type0.common.status.DEVSELTiming = 0x2,
    .type0.common.status.signalTargetAbort = 0x0,
    .type0.common.status.rcvdTargetAbort = 0x0,
    .type0.common.status.rcvdMasterAbort = 0x0,
    .type0.common.status.signalledSystemErr = 0x0,
    .type0.common.status.detectParityErr = 0x0,
    .type0.common.revisionID = AXP_M1543C_REV_ID,       /* Index 08h, RO */
    .type0.common.programmingInterface = 0x00,          /* Index 0bh-09h, RO */
    .type0.common.subclass = AXP_PCI_06_SUB_ISA,
    .type0.common.class = AXP_PCI_CLASS_BRIDGE,
    .type0.common.cacheLineSize = 0x00,                 /* Index 0dh-0ch, RO */
    .type0.common.latencyTimer = 0x00,
    .type0.common.headerType.headerType = 0x00,         /* Index 0eh, RO */
    .type0.common.bist.BIST = 0x00,                     /* Index 2bh-0fh, RO */
    .type0.bar[0].BAR = 0,
    .type0.bar[1].BAR = 0,
    .type0.bar[2].BAR = 0,
    .type0.bar[3].BAR = 0,
    .type0.bar[4].BAR = 0,
    .type0.bar[5].BAR = 0,
    .type0.cardbusCISPtr = 0,
    .type0.subsystemVendorID = 0x0000,                  /* Index 2dh-2ch, RW */
    .type0.subsystemID = 0x0000,                        /* Index 2fh-2eh, RW */
    .type0.ROMBaseAddr = 0,                             /* Index 3fh-30h, RO */
    .type0.capabilitiesPtr = 0x00,
    .type0.res_0[0] = 0x00,
    .type0.res_0[1] = 0x00,
    .type0.res_0[2] = 0x00,
    .type0.res_1 = 0,
    .type0.interruptLine = 0x00,
    .type0.interruptPIN = 0x00,
    .type0.minGrant = 0x00,
    .type0.maxLatency = 0x00,
    .config[16] = 0,                                    /* Index 43h-40h */
    .config[17] = 0,                                    /* Index 47h-44h */
    .config[18] = 0,                                    /* Index 4bh-48h */
    .config[19] = 0,                                    /* Index 4fh-4ch */
    .config[20] = 0,                                    /* Index 53h-50h */

    /* Programmable chip select (pin PCSJ) address define. */
    .config[21] = 0x00000200,                           /* Index 57h-54h, RO */
    .config[22] = 0,                                    /* Index 5bh-58h */
    .config[23] = 0,                                    /* Index 5fh-5ch */
    .config[24] = 0,                                    /* Index 63h-60h */
    .config[25] = 0,                                    /* Index 67h-64h */
    .config[26] = 0,                                    /* Index 6bh-68h */
    .config[27] = 0,                                    /* Index 6fh-6ch */
    .config[28] = 0,                                    /* Index 73h-70h */
    .config[29] = 0,                                    /* Index 77h-74h */
    .config[30] = 0,                                    /* Index 7bh-78h */
    .config[31] = 0,                                    /* Index 7fh-7ch */
    .config[32] = 0,                                    /* Index 83h-80h */
    .config[33] = 0,                                    /* Index 87h-84h */
    .config[34] = 0,                                    /* Index 8bh-88h */
    .config[35] = 0,                                    /* Index 8fh-8ch */
    .config[36] = 0,                                    /* Index 93h-90h */
    .config[37] = 0,                                    /* Index 97h-94h */
    .config[38] = 0,                                    /* Index 9bh-98h */
    .config[39] = 0,                                    /* Index 9fh-9ch */
    .config[40] = 0,                                    /* Index a3h-a0h */
    .config[41] = 0,                                    /* Index a7h-a4h */
    .config[42] = 0,                                    /* Index abh-a8h */
    .config[43] = 0,                                    /* Index afh-ach */
    .config[44] = 0,                                    /* Index b3h-b0h */
    .config[45] = 0,                                    /* Index b7h-b4h */
    .config[46] = 0,                                    /* Index bbh-b8h */
    .config[47] = 0,                                    /* Index bfh-bch */
    .config[48] = 0,                                    /* Index 63h-c0h */
    .config[49] = 0,                                    /* Index 67h-c4h */
    .config[50] = 0,                                    /* Index cbh-c8h */
    .config[51] = 0,                                    /* Index cfh-cch */
    .config[52] = 0,                                    /* Index d3h-d0h */
    .config[53] = 0,                                    /* Index d7h-d4h */
    .config[54] = 0,                                    /* Index dbh-d8h */
    .config[55] = 0,                                    /* Index dfh-dch */
    .config[56] = 0,                                    /* Index e3h-e0h */
    .config[57] = 0,                                    /* Index e7h-e4h */
    .config[58] = 0,                                    /* Index ebh-e8h */
    .config[59] = 0,                                    /* Index efh-ech */
    .config[60] = 0,                                    /* Index f3h-f0h */
    .config[61] = 0,                                    /* Index f7h-f4h */
    .config[62] = 0,                                    /* Index fbh-f8h */
    .config[63] = 0                                     /* Index ffh-fch */
};

static const AXP_PCI_ConfigData mask =
{
    .config[0] = 0,
    .config[1] = 0, /* bits 13 and 12 of high word cleared by writing 1 to them */
    .config[2] = 0,
    .config[3] = 0,
    .config[4] = 0,
    .config[5] = 0,
    .config[6] = 0,
    .config[7] = 0,
    .config[8] = 0,
    .config[9] = 0,
    .config[10] = 0,
    .config[11] = 0,
    .config[12] = 0,
    .config[13] = 0,
    .config[14] = 0,
    .config[15] = 0,
    .config[16] = 0xffcfff7f,
    .config[17] = 0xff00cbdf,
    .config[18] = 0xffffffff,
    .config[19] = 0x000000ff,
    .config[20] = 0xcfff8fff,
    .config[21] = 0xe0ffff00,
    .config[22] = 0x020f0d7f,
    .config[23] = 0xffe0027f,
    .config[24] = 0,
    .config[25] = 0,
    .config[26] = 0,
    .config[27] = 0x00ffbf00,
    .config[28] = 0xffefefff,
    .config[29] = 0x1fffffdf,
    .config[30] = 0,
    .config[31] = 0,
    .config[32] = 0,
    .config[33] = 0,
    .config[34] = 0,
    .config[35] = 0,
    .config[36] = 0,
    .config[37] = 0,
    .config[38] = 0,
    .config[39] = 0,
    .config[40] = 0,
    .config[41] = 0,
    .config[42] = 0,
    .config[43] = 0,
    .config[44] = 0,
    .config[45] = 0,
    .config[46] = 0,
    .config[47] = 0,
    .config[48] = 0,
    .config[49] = 0,
    .config[50] = 0,
    .config[51] = 0,
    .config[52] = 0,
    .config[53] = 0,
    .config[54] = 0,
    .config[55] = 0,
    .config[56] = 0,
    .config[57] = 0,
    .config[58] = 0,
    .config[59] = 0,
    .config[60] = 0,
    .config[61] = 0,
    .config[62] = 0,
    .config[63] = 0
};

static AXP_Ali_1543C device_blk;

/*
 * AXP_Ali_M1543C_configRead
 *  This function is called to read the configuration data based on the
 *  function, register, offset, and up to a length of size.  This is a single
 *  function device, therefore the func parameter is ignored.
 *
 * Input Parameters:
 *  func:
 *      A value indicating the function configuration to be read.
 *  reg:
 *      A value indicating the register to be read.
 *  offset:
 *      A value indicating the offset, in bytes, within the register to be read.
 *  size:
 *      A value indicating the number of bytes to be read.  Can be 1, 2, or 4.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  The value of the requested configuration data.
 */
u32
AXP_Ali_M1543C_configRead(u8 func, u8 reg, u8 offset, u8 size)
{
    u8 *data;
    u32 retVal = 0;

    if (func == 0)
    {

        /*
         * First get the address of where the register we are trying to read
         * is located.
         */
        data = (u8 *) &device_blk.currentConfig.config[reg];

        /*
         * Mask out the not requested bits.
         */
        retVal = MASK_DATA_32(&data[offset], (size * 8));
    }
    else
    {
        retVal = 0xffffffff;    /* same as -1 */
    }

    /*
     * Log that we are returning configuration data, if logging is turned on.
     */
    if (AXP_SYS_OPT2)
    {
        AXP_TRACE_BEGIN();
        AXP_TraceWrite("Ali M1543C(function: %d) config read %d bytes @ "
                       "register %d[%d] = %08x\n", func, size, reg, offset,
                       retVal);
        AXP_TRACE_END();
    }

    /*
     * Return the results to the caller.
     */
    return (retVal);
}

/*
 * AXP_Ali_M1543C_configWrite
 *  This function is called to write configuration data based on the
 *  function, register, offset, and up to a length of size.  This is a single
 *  function device, therefore the func parameter is ignored.
 *
 *  NOTE: Read-only fields will be merged in unchanged.
 *
 * Input Parameters:
 *  func:
 *      A value indicating the function configuration to be read.
 *  reg:
 *      A value indicating the register to be read.
 *  offset:
 *      A value indicating the offset, in bytes, within the register to be read.
 *  data:
 *      An up to 32-bit (4 byte) data value to be written to the configuration.
 *  size:
 *      A value indicating the number of bytes to be written.  Can be 1, 2, or
 *      4.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 *
 * NOTE: This function does not allow a value in the data to be written across
 *       a 32-bit boundary.  Therefore, the combination of offset and size
 *       cannot be greater than 4.
 */
void
AXP_Ali_M1543C_configWrite(u8 func, u8 reg, u8 offset, u32 data, u8 size)
{
    AXP_PCI_ConfigData *config = &device_blk.currentConfig;
    u32 *regData = &config[offset];
    u32 regMask = mask[offset];
    u32 newData;

    if (func == 0)
    {

        /*
         * We need to special case the Status Register at index 07h-06h.  The
         * size should only be 2 bytes long.  Specifying a 1 in the data for
         * these bits causes the bits to be cleared, even if they are already
         * cleared.
         */
        if ((reg == AXP_PCI_STATUS_INDEX) &&
            (size == sizeof(config->type0.common.status)))
        {
            if (data & AXP_PCI_STATUS_PERR_BIT)
            {
                config->type0.common.status.detectedParityErr = 0;
            }
            if (data & AXP_PCI_STATUS_SERR_BIT)
            {
                config->type0.common.status.signalledSystemErr = 0;
            }
            if (data & AXP_PCI_STATUS_RMABT_BIT)
            {
                config->type0.common.status.rcvdMasterAbort = 0;
            }
            if (data & AXP_PCI_STATUS_RTABT_BIT)
            {
                config->type0.common.status.rcvdTargetAbort = 0;
            }
            if (data & AXP_PCI_STATUS_STABT_BIT)
            {
                config->type0.common.status.signalTargetAbort = 0;
            }
        }
        else
        {

            /*
             * In order to merge in the data we need to make sure that we do
             * not overwrite read-only data.  In order to do this we perform
             * the following steps:
             *
             *  1) Make sure the bits not to be copied in the data are all
             *     zero.
             *  2) Shift the data bits remaining into their proper offset
             *     location.
             *  3) Make sure the writable bits at the offset and length are all
             *     zero.
             *  4) Make sure only the read-write bits in the data are set.
             *  5) Clear out the bits to be written, ignoring read-only bits,
             *     from the current configuration data.
             *  6) Perform a logical OR on the current configuration data and
             *     the data to be written.
             */
            data = MASK_DATA_32(&data, size);
            newData = data << (offset * 8);
            regMask = MASK_IN_32(regMask, offset, size);
            newData &= regMask;
            *regData &= !regMask;
            *regData |= newData;
        }

        /*
         * Log that we have written configuration data, if logging is turned
         * on.
         */
        if (AXP_SYS_OPT2)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("Ali M1543C(function: %d) config wrote %d bytes @ "
                           "register %d[%d] = %08x\n", func, size, reg, offset,
                           data);
            AXP_TRACE_END();
        }
    }
    else
    {

        /*
         * Log that we have written configuration data, if logging is turned
         * on.
         */
        if (AXP_SYS_OPT2)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("Ali M1543C(function: %d) invalid function\n", func);
            AXP_TRACE_END();
        }
    }

    /*
     * Return the results to the caller.
     */
    return;
}

/*
 * AXP_Ali_M1543C_init
 *  This function is called by the Pchip when the Ali M1543C PCI device is
 *  configured to the emulated system.  This function initialize the current
 *  configuration from the base and calls back into the Pchip to register
 *  itself.
 *
 * Input Parameters:
 *  p:
 *      A pointer to the Pchip structure.
 *  dev_id:
 *      A value indicating the device ID to be assigned to this device.  If it
 *      is AXP_PCI_MAX_DEVICES, then the next available device location will be
 *      selected.
 *
 * Output Parameters:
 *  None,
 *
 * Return Values:
 *  None
 */
void
AXP_Ali_M1543C_init(AXP_21274_PCHIP *p, u32 dev_id)
{
    u8 *config = (u8 *) device_blk.currentConfig.config;
    int ii;

    /*
     * Copy the initial configuration to the current.
     */
    for (ii = 0; ii < sizeof(AXP_PCI_ConfigData); ii++)
    {
        config[ii] = configBase.config[ii];
    }

    /*
     * Initialize nmi register and rtc ports.
     */
    device_blk.nmi_status_control_reg = 0;
    for (ii = 0; ii < ALI_RTC_PORTS; ii++)
    {
        device_blk.rtc_ports[ii] = 0;
    }

    /*
     * TODO: We have to configure in the TOY clock.
     */

    device_blk.devId = AXP_21274_Pchip_Register(p,
                                                dev_id,
                                                AXP_Ali_M1543C_configRead,
                                                AXP_Ali_M1543C_configWrite,
                                                NULL,     /* ioRead */
                                                NULL);    /* ioWrite */

    if (dev_id == AXP_PCI_MAX_DEVICES)
    {
        if (AXP_SYS_CALL)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("Aladdin M1543C Failed to be allocated a PCI slot"
                           " in Pchip #%d", p->pChipID);
            AXP_TRACE_END();
        }
    }
    else
    {
        if (AXP_SYS_CALL)
        {
            AXP_TRACE_BEGIN();
            AXP_TraceWrite("Aladdin M1543C allocated a PCI slot #%d"
                           " in Pchip #%d", dev_id, p->pChipID);
            AXP_TRACE_END();
        }
    }
}

/*
 * Parallel Port Information: M1543C DS Pages 181-182
 *
 * DATA                                                 Address Offset = 00H
 *
 *  Data Port
 *
 *  The Data Port is located at an offset of '00h' from the base address. The
 *  data register is cleared at initialization by RESET.  During a WRITE
 *  operation, the Data Register latches the contents of the data bus on the
 *  rising edge of the IOWJ input. The contents of this register are buffered
 *  (non inverting) and output onto the PD0 - PD7 ports. During a READ
 *  operation, PD0 - PD7 ports are read and output to the host CPU.
 *
 * Device Status Register (DSR)                         Address Offset = 01H
 *
 *  The Status Port is located at an offset of '01h' from the base address.
 *  Bits 0 - 2 are not implemented as register bits, during a read of the
 *  Printer Status Register these bits are a low level. The bits of the Status
 *  port are defined as follows:
 *      BIT 3 FaultJ    - The level on the Fault input is read by the CPU as
 *                        bit 3 of the Device Status Register.
 *      BIT 4 Select    - The level on the Select input is read by the CPU as
 *                        bit 4 of the Device Status Register.
 *      BIT 5 PError    - The level on the PError input is read by the CPU as
 *                        bit 5 of the Device Status Register. Printer Status
 *                        Register.
 *      BIT 6 AckJ      - The level on the AckJ input is read by the CPU as bit
 *                        6 of the Device Status Register.
 *      BIT 7 BusyJ     - The complement of the level on the BUSY input is read
 *                        by the CPU as bit 7 of the Device Status Register.
 *
 * Device Control Register (DCR)                        Address Offset = 02H
 *
 *  The Control Register is located at an offset of '02H' from the base
 *  address. The Control Register is initialized to zero by the RESET input,
 *  bits 0 to 5 only being affected; bits 6 and 7 are hard wired low.
 *
 *      BIT 0 STROBE    - This bit is inverted and output onto the STROBEJ
 *                        output.
 *      BIT 1 AUTOFEED  - This bit is inverted and output onto the AUTOFDJ
 *                        output. A logic 1 causes the printer to generate a
 *                        line feed after each line is printed. A logic 0 means
 *                        no autofeed.
 *      BIT 2 INITIATE OUTPUT - This bit is output onto the INITJ output
 *                        without inversion.
 *      BIT 3 SELECT    - This bit is inverted and output onto the SLCTINJ
 *                        output. A logic 1 on this bit selects the printer; a
 *                        logic 0 means the printer is not selected.
 *      BIT 4 INTERRUPT REQUEST ENABLE - The interrupt request enable bit when
 *                        set to a high level may be used to enable interrupt
 *                        requests from the Parallel Port to the CPU due to a
 *                        low to high transition on the ACKJ input. Refer to
 *                        the description of the interrupt under Operation,
 *                        Interrupts.
 *      BIT 5 DIRECTION - This bit has no effect and the direction is always
 *                        out regardless of the state of this bit.
 *      Bits 6 and 7 during a read are a low level, and cannot be written.
 *
 * NOTE: We only support Extended Control Register (ECR) mode 001 (PS/2
 *       Parallel Port mode)
 */

/*
 * AXP_Ali_Printer_Reset
 *  This function is called to reset the printer registers to their initial
 *  values.
 *
 * Input Parameters:
 *  None.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_Ali_Printer_Reset()
{
    device_blk.printer_data = 0xff;     /* Printer data to all 1s */

    /* Device status to: busy, ack, online, and fault */
    device_blk.printer_status.res_1 = 0;
    device_blk.printer_status.fault = 1;
    device_blk.printer_status.select = 1;
    device_blk.printer_status.error = 0;
    device_blk.printer_status.ack = 1;
    device_blk.printer_status.not_busy = 1;

    /* Device control to: initiate and select */
    device_blk.printer_ctrl.strobe = 0;
    device_blk.printer_ctrl.autofeed = 0;
    device_blk.printer_ctrl.initiate = 1;
    device_blk.printer_ctrl.select = 1;
    device_blk.printer_ctrl.interrupt_req = 0;
    device_blk.printer_ctrl.direction = 0;  /* only value supported */
    device_blk.printer_ctrl.res_1 = 0;
}

/*
 * AXP_Ali_Printer_Read
 *  This function is called to read one of the printer registers.  The printer
 *  registers are: data, status, and control.
 *
 * Input Parameters:
 *  offset:
 *      This is the offset from the base address.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  The value currently in the data, status, or control registers.
 *
 * Side Effect:
 *  If the status register is read, then if the not_busy in the status and
 *  strobe in the control registers are clear, then the ack in the status
 *  register will be inverted.  If the ack was set, then the not_busy flag
 *  will be set.
 */
static u8
AXP_Ali_Printer_Read(u32 offset)
{
    u8 retVal = 0;

    switch (offset)
    {
        /* Return what is in the data register */
        case 0:
            retVal = device_blk.printer_data;
            break;

        /* Return what is in the status register */
        case 1:
            retVal = device_blk.printer_status.status;
            if ((device_blk.printer_status.not_busy == 0) &&
                (device_blk.printer_ctrl.strobe == 0))
            {
                if (device_blk.printer_status.ack)
                {
                    device_blk.printer_status.ack = 0;
                }
                else
                {
                    device_blk.printer_status.ack = 1;
                    device_blk.printer_status.not_busy = 1;
                }
            }
            break;

        /* Return what is in the control register */
        case 2:
            retVal = device_blk.printer_ctrl.ctrl;
            break;

        default:
            break;
    }
    return (retVal);
}

/*
 * AXP_Ali_Printer_Write
 *  This function is called to write data to either the data register or the
 *  control register.
 *
 * Input Parameters:
 *  offset:
 *      This is the offset from the base address.
 *  data:
 *      This is the data to be written to the appropriate register.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
static void
AXP_Ali_Printer_Write(u32 offset, u8 data)
{
    AXP_Ali_Device_Control_Register ctrl_data = {.ctrl = data};

    switch (offset)
    {
        case 0:
            device_blk.printer_data = data;
            break;

        case 2:
            if (ctrl_data.initiate)
            {
                device_blk.printer_status.res_1 = 0;
                device_blk.printer_status.fault = 1;
                device_blk.printer_status.select = 1;
                device_blk.printer_status.error = 0;
                device_blk.printer_status.ack = 1;
                device_blk.printer_status.not_busy = 1;
            }
            else if (ctrl_data.select)
            {
                if (ctrl_data.strobe)
                {
                    device_blk.printer_status.not_busy = 0;

                    if (ctrl_data.initiate)
                    {
                        /* Do the write to the printer */
                    }
                    if (ctrl_data.interrupt_req)
                    {
                        /* Do an interrupt controller interrupt */
                    }
                }
            }
            device_blk.printer_ctrl.ctrl = data;
            break;

        default:
            break;
    }
}
