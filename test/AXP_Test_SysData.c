/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This test program contains the code used to exercise the functionality
 *  provided by the AXP_SysData functions.
 *
 * Revision History:
 *
 *  V01.000 26-Jan-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "com-util/AXP_SysDataDefs.h"

typedef enum
{
    Rq_NoOp,
    Rq_Add,
    Rq_Remove,
    Rq_Swap,
    Rq_Stop
} Request;

typedef struct
{
    pthread_mutex_t *mutex;
    pthread_cond_t *cond;
    Request *rq;
    u64 id;
} ThreadData;

static AXP_SysData_Buffer buf;

/*
 * main
 *  This is the main program that is going to be used to test the SysData
 *  functionality.  The code will perform the following steps:
 *
 *      1)  Create a SysData List containing 0 entries
 *          (should return a null pointer).
 *      2)  Create a SysData List containing 1 entry
 *          (a valid pointer should be returned).
 *      3)  Create a second SysData List containing 4 entries
 *          (a valid pointer should be returned).
 *      4)  Delete all allocated lists.
 *          (the address returned should be set to NULL)
 *      5)  The following is performed in a single thread:
 *          a)  Add a buffer between 0 and 64 bytes to the second SysData list
 *              (should return a non-zero value).
 *          b)  Add a buffer of more than 64 bytes to the first SysData list
 *              (should return a zero value).
 *          c)  Add a buffer of 64 bytes to the first SysData list
 *              (should return a non-zero value).
 *          d)  Add a second buffer of 64 bytes to the first SysData list
 *              (should return a zero value -- list full).
 *          e)  Add a second buffer between 0 and 64 bytes to the second
 *              SysData list
 *              (should return a non-zero value).
 *          f)  Swap the first SysData list buffer to the second SysData list
 *              buffer, specifying an id the is not in the first list.
 *              (should return a false and the list should remain as they
 *              were).
 *          g)  Swap one of the second SysData list buffers with the first
 *              (should return a false -- first list is full and neither list
 *              should be modified).
 *          h)  Swap the first SysData list buffer with the second
 *              (should return a true -- the buffer in the first list should
 *              now be in the second SysData list).
 *          i)  Add a buffer of 64 bytes to the first SysData list
 *              (should return a non-zero value -- step h should have made the
 *              first list have room for another entry).
 *          j)  Remove the first SysData list buffer but specify an id that
 *              does not exist in the list.
 *              (the length field should indicate a zero length).
 *          k)  Remove the first SysData list buffer specifying an id that
 *              does exist in the list.
 *              (the length field should indicate the number of bytes returned
 *              and the buffer should contain the original data stored).
 *          l)  Remove the first SysData list buffer specifying the id that
 *              we just removed.
 *              (the length field should indicate a zero length).
 *      6)  The following is performed in multiple threads (after setup):
 *          a)  Perform the creation of 4 SysData lists.
 *          b)  Create a request location, condition variable and mutex to be
 *              used to communicate with the threads to be created next.
 *          c)  Create a thread for each created SysData list
 *              -- Each thread will wait on it's condition and then perform the
 *                 indicated request.
 *          d)  The main thread will, in a finite loop, randomly generate
 *              requests and send them to random threads (keeping track that
 *              the lists do not get full).
 *              -- No deadlocks should occur and data should be moved around
 *                 as expected.
 *          e)  The main thread will tell the other threads to shutdown and
 *              for them all to complete.
 *          f)  The main thread will clean-up all the lists and exit with a
 *              pass or fail indication and return value.
 */
int
main()
{
    AXP_SysData_List *lists[4];
    AXP_SysData_Buffer outBuf;
    u32 outLen;
    int ii, id1, id2, id3, id4;
    int passed = 0;
    int failed = 0;
    int tests = 0;

    /*
     * First, initialize the data buffer that will be used throughout the
     * tests.
     */
    for (ii = 0; ii < AXP_SYSDATA_BUFLEN; ii++)
    {
        buf[ii] = rand();
    }

    /*
     * Also initialize all the SysData Lists pointing to NULL.
     */
    for (ii = 0; ii < 4; ii++)
    {
        lists[ii] = NULL;
    }

    /*
     * 1)  Create a SysData List containing 0 entries
     *     (should return a null pointer).
     */
    printf("AXP_SysData_Create with a zero length...");
    fflush(stdin);
    tests++;
    lists[0] = AXP_SysData_Create(0);
    if (lists[0] == NULL)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     * 2)  Create a SysData List containing 1 entry
     *     (a valid pointer should be returned).
     */
    printf("AXP_SysData_Create with a length of one...");
    fflush(stdin);
    tests++;
    lists[0] = AXP_SysData_Create(1);
    if ((lists[0] != NULL) && (lists[0]->list_size == 1) &&
        (lists[0]->first_free == lists[0]->first) && (lists[0]->id != 0) &&
        (lists[0]->first != NULL) && (lists[0]->first->blink != NULL) &&
        (lists[0]->first->flink == NULL) && (lists[0]->first->valid == false))
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     * 3)  Create a second SysData List containing 4 entries
     *     (a valid pointer should be returned).
     */
    printf("AXP_SysData_Create with a length of four...");
    fflush(stdin);
    tests++;
    lists[1] = AXP_SysData_Create(4);
    if ((lists[1] != NULL) && (lists[1]->list_size == 4) &&
        (lists[1]->first_free == lists[1]->first) && (lists[1]->id != 0) &&
        ((lists[1]->first != NULL) && (lists[1]->first->valid == false)) &&
        ((lists[1]->first->flink != NULL) &&
         (lists[1]->first->flink->valid == false)) &&
        ((lists[1]->first->flink->flink != NULL) &&
         (lists[1]->first->flink->flink->valid == false)) &&
         ((lists[1]->first->flink->flink->flink != NULL) &&
          (lists[1]->first->flink->flink->flink->valid == false)) &&
         (lists[1]->first->flink->flink->flink->flink == NULL))
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     * 4)  Delete all allocated lists.
     *     (the address returned should be set to NULL)
     */
    printf("AXP_SysData_Delete of list with a length of one...");
    fflush(stdin);
    tests++;
    AXP_SysData_Delete(&lists[0]);
    if (lists[0] == NULL)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    printf("AXP_SysData_Delete of list with a length of four...");
    fflush(stdin);
    tests++;
    AXP_SysData_Delete(&lists[1]);
    if (lists[1] == NULL)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     * We don't count the next calls as tests, since we are just setting up for
     * the next set of tests.
     */
    lists[0] = AXP_SysData_Create(1);
    lists[1] = AXP_SysData_Create(4);

    /*
     * 5)  The following is performed in a single thread:
     *   a)  Add a buffer between 0 and 64 bytes to the second SysData list
     *       (should return a non-zero value).
     */
    printf("AXP_SysData_Add to list with a length of four...");
    fflush(stdin);
    tests++;
    id1 = AXP_SysData_Add(lists[1], buf, AXP_SYSDATA_BUFLEN);
    if (id1 != 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   b)  Add a buffer of more than 64 bytes to the second SysData list
     *       (should return a zero value).
     */
    printf("AXP_SysData_Add too large buffer to list with a length of four...");
    fflush(stdin);
    tests++;
    id2 = AXP_SysData_Add(lists[1], buf, AXP_SYSDATA_BUFLEN+1);
    if (id2 == 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   c)  Add a buffer of 64 bytes to the first SysData list
     *       (should return a non-zero value).
     */
    printf("AXP_SysData_Add to list with a length of one...");
    fflush(stdin);
    tests++;
    id2 = AXP_SysData_Add(lists[0], buf, AXP_SYSDATA_BUFLEN);
    if (id2 != 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   d)  Add a second buffer of 64 bytes to the first SysData list
     *       (should return a zero value -- list full).
     */
    printf("AXP_SysData_Add to list with a length of one...");
    fflush(stdin);
    tests++;
    id3 = AXP_SysData_Add(lists[0], buf, AXP_SYSDATA_BUFLEN);
    if (id3 == 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   e)  Add a second buffer between 0 and 64 bytes to the second SysData
     *       list
     *       (should return a non-zero value).
     */
    printf("AXP_SysData_Add to list with a length of four...");
    fflush(stdin);
    tests++;
    id3 = AXP_SysData_Add(lists[1], buf, AXP_SYSDATA_BUFLEN/2+1);
    if (id3 != 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   f)  Swap the first SysData list buffer to the second SysData list
     *       buffer, specifying an id the is not in the first list.
     *       (should return a false and the list should remain as they were).
     */
    printf("AXP_SysData_Swap from list of one to list of four...");
    fflush(stdin);
    tests++;
    if ((AXP_SysData_Swap(lists[0], 0x100000000, lists[1]) == false) &&
        (lists[0]->first_free == NULL))
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   g)  Swap one of the second SysData list buffers with the first
     *       (should return a false -- first list is full and neither list
     *       should be modified).
     */
    printf("AXP_SysData_Swap from list of four to list of one...");
    fflush(stdin);
    tests++;
    if ((AXP_SysData_Swap(lists[1], id1, lists[0]) == false) &&
        (lists[0]->first_free == NULL))
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   h)  Swap the first SysData list buffer with the second
     *       (should return a true -- the buffer in the first list item should
     *       now be in the second SysData list).
     */
    printf("AXP_SysData_Swap another from list of one to list of four...");
    fflush(stdin);
    tests++;
    if ((AXP_SysData_Swap(lists[0], id2, lists[1]) == true) &&
        (lists[0]->first_free == (AXP_SysData_Block *) lists[0]->first))
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   i)  Add a buffer of 64 bytes to the first SysData list
     *       (should return a non-zero value -- step h should have made the
     *       first list have room for another entry).
     */
    printf("AXP_SysData_Add to the list of one...");
    fflush(stdin);
    tests++;
    id4 = AXP_SysData_Add(lists[0], buf, AXP_SYSDATA_BUFLEN);
    if (id4 != 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   j)  Remove the first SysData list buffer but specify an id that does
     *       not exist in the list.
     *       (the length field should indicate a zero length).
     */
    printf("AXP_SysData_Remove bad id from the list of one...");
    fflush(stdin);
    tests++;
    AXP_SysData_Remove(lists[0], 0x10000000, outBuf, &outLen);
    if (outLen == 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   k)  Remove the first SysData list buffer specifying an id that does
     *       exist in the list.
     *       (the length field should indicate the number of bytes returned and
     *       the buffer should contain the original data stored).
     */
    printf("AXP_SysData_Remove good id from the list of four...");
    fflush(stdin);
    tests++;
    AXP_SysData_Remove(lists[1], id2, outBuf, &outLen);
    if ((outLen == AXP_SYSDATA_BUFLEN) &&
        (memcmp(buf, outBuf, outLen) == 0))
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     *   l)  Remove the first SysData list buffer specifying the id that we just
     *       removed.
     *       (the length field should indicate a zero length).
     */
    printf("AXP_SysData_Remove same id from the list of four...");
    fflush(stdin);
    tests++;
    AXP_SysData_Remove(lists[1], id2, outBuf, &outLen);
    if (outLen == 0)
    {
        printf("passed\n");
        passed++;
    }
    else
    {
        printf("failed\n");
        failed++;
    }

    /*
     * Cleanup after ourselves.
     */
    AXP_SysData_Delete(&lists[0]);
    AXP_SysData_Delete(&lists[1]);

    /*
     * Print out the results.
     */
    printf("\nTests executed: %d\n", tests);
    printf("Tests passed:   %d\n", passed);
    printf("Tests failed:   %d\n", failed);
    return(failed != 0 ? -1 : 0);
}
